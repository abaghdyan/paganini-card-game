﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Threading;
using Microsoft.Win32;
using Paganini.Enums;

namespace Paganini
{
    delegate void GameStageChangedEventHandler(GameStages stage);
    delegate void GameActivationChangedEventHandler(bool isActivated);
    delegate void GameSoundChangedEventHandler(bool isSoundOn);
    public partial class MainWindow : Window
    {
        static public bool IsGameActivated
        {
            get { return _isGameActivated; }
            set
            {
                _isGameActivated = value;
                ActivationStatusChanged?.Invoke(_isGameActivated);
            }
        }

        public bool IsSoundOn
        {
            get { return _isSoundOn; }
            set
            {
                _isSoundOn = value;
                GameSoundChanged?.Invoke(_isSoundOn);
            }
        }

        public bool ShowCurrentTime
        {
            get { return _showCurrentTime; }
            set
            {
                _showCurrentTime = value;
                if (_showCurrentTime)
                {
                    TimeLabel.Visibility = Visibility.Visible;
                    realTimeTimer.Start();
                }
                else
                {
                    TimeLabel.Visibility = Visibility.Hidden;
                    realTimeTimer.Stop();
                }
            }
        }

        private GameStages GameStage
        {
            get { return _gameStage; }
            set
            {
                _gameStage = value;
                GameStageChanged?.Invoke(_gameStage);
            }
        }
        public GameRecordMode RecordMode
        {
            get
            {
                return _recordMode;
            }
            set
            {
                _recordMode = value;
                switch (_recordMode)
                {
                    case GameRecordMode.None:
                        checkMove.Visibility = Visibility.Hidden;
                        checkTime.Visibility = Visibility.Hidden;
                        break;
                    case GameRecordMode.Move:
                        checkMove.Visibility = Visibility.Visible;
                        break;
                    case GameRecordMode.Time:
                        checkTime.Visibility = Visibility.Visible;
                        break;
                    default:
                        break;
                }
            }
        }

        public int RemainingCardsShuffle
        {
            get
            {
                return _remainingCardsShuffle;
            }
            set
            {
                _remainingCardsShuffle = value;
                remainingShuffle.Header = $"Shuffle remaining cards({_remainingCardsShuffle})";
                if (_remainingCardsShuffle > 0)
                    remainingShuffle.IsEnabled = true;
                else
                    remainingShuffle.IsEnabled = false; ;
            }
        }
        public string UserName { get; set; }
        private Card SelectedCard
        {
            get { return _selectedCard; }
            set
            {
                if (_selectedCard != null)
                {
                    _selectedCard.MouseUp -= Image_MouseUp;
                    _selectedCard.ToOldPlace();
                }
                _selectedCard = value;
            }
        }

        SoundPlayer correctSound;
        SoundPlayer errorSound;

        DispatcherTimer realTimeTimer = new DispatcherTimer();
        DispatcherTimer gameTimeTimer = new DispatcherTimer();
        DispatcherTimer remainingTimeTimer = new DispatcherTimer();

        BlurEffect newGameBLur = new BlurEffect();

        double xD;
        double yD;
        Random rnd;
        int cardIndex = 0;

        int _moveCount;
        int _gameTime;
        int _remainingTime = 1000;
        int _remainingCardsShuffle = 2;

        private event GameStageChangedEventHandler GameStageChanged;
        static private event GameActivationChangedEventHandler ActivationStatusChanged;
        private event GameSoundChangedEventHandler GameSoundChanged;

        List<Card> cards = new List<Card>(52);
        List<BlankSpace> blankSpaces = new List<BlankSpace>(4);
        Stack<Move> moveJournal = new Stack<Move>();

        public Command NewGameCommand { get; set; }
        public Command ShuffleGameCommand { get; set; }
        public Command Undo_ClickCommand { get; set; }


        static private bool _isGameActivated;
        private Card _selectedCard;
        private GameStages _gameStage;
        private bool _isSoundOn;
        private bool _showCurrentTime;
        private GameRecordMode _recordMode;

        public MainWindow()
        {
            NewGameCommand = new Command(NewGame);
            ShuffleGameCommand = new Command(ShuffleRemainingCards);
            Undo_ClickCommand = new Command(Undo);

            InitializeComponent();

            MainGrid.MouseMove += Image_MouseMove;
            GameStageChanged += MainWindow_GameStageChanged;
            ActivationStatusChanged += MainWindow_ActivationStatusChanged;
            GameSoundChanged += MainWindow_GameSoundChanged;

            newGameBLur.Radius = 5;
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey isAvailable = currentUserKey.OpenSubKey("PaganiniRecordInfo");
            if (isAvailable == null)
            {
                RegistryKey PaganiniRecordInfo = currentUserKey.CreateSubKey("PaganiniRecordInfo");
                RegistryKey TimeRecordInfo = PaganiniRecordInfo.CreateSubKey("TimeRecordInfo");
                TimeRecordInfo.SetValue("TimeRecord", GetBytes("999999"));
                TimeRecordInfo.SetValue("TimeRecordsPerson", GetBytes("Unknown"));
                RegistryKey MovesRecordInfo = PaganiniRecordInfo.CreateSubKey("MovesRecordInfo");
                MovesRecordInfo.SetValue("MovesRecord", GetBytes("999999"));
                MovesRecordInfo.SetValue("MovesRecordsPerson", GetBytes("Unknown"));
                PaganiniRecordInfo.Close();
            }

            GameStage = GameStages.CardsDealing;
            RecordMode = GameRecordMode.None;
            IsSoundOn = true;
            ShowCurrentTime = true;

            realTimeTimer.Tick += new EventHandler(realTimeTick);
            realTimeTimer.Interval = new TimeSpan(0, 0, 1);
            realTimeTimer.Start();

            gameTimeTimer.Tick += new EventHandler(gameTimeTick);
            gameTimeTimer.Interval = new TimeSpan(0, 0, 1);

            remainingTimeTimer.Tick += new EventHandler(remainingTimeTick);
            remainingTimeTimer.Interval = new TimeSpan(0, 0, 1);

            List<Ranks> Rankss = new List<Ranks>(13);

            for (int j = 0; j < 13; j++)
            {
                Rankss.Add((Ranks)j);
            }
            Suits[] Suitss = new Suits[4];
            for (int j = 0; j < Suitss.Length; j++)
            {
                Suitss[j] = (Suits)j;
            }
            foreach (Ranks r in Rankss)
            {
                foreach (Suits s in Suitss)
                {
                    var card = new Card(r, s);
                    cards.Add(card);
                    card.Source = card.GetSource();
                    card.Visibility = Visibility.Hidden;
                    card.MouseDown += Image_MouseDown;
                    MainGrid.Children.Add(card);
                }
            }

            foreach (Card card in cards)
            {
                card.PreviousCard = cards.FirstOrDefault(c => c.Suit == card.Suit && c.Rank + 1 == card.Rank);
                card.NextCard = cards.FirstOrDefault(c => c.Suit == card.Suit && c.Rank - 1 == card.Rank);
            }

            for (int j = 0; j < 4; j++)
            {
                BlankSpace blankSpace = new BlankSpace();
                blankSpaces.Add(blankSpace);
                MainGrid.Children.Add(blankSpace);
            }
        }


        private void MainWindow_GameSoundChanged(bool isSoundOn)
        {
            if (isSoundOn)
            {
                correctSound = new SoundPlayer(Properties.Resources.correct);
                errorSound = new SoundPlayer(Properties.Resources.error);
            }
            else
            {
                correctSound = null;
                errorSound = null;
            }
        }

        private void MainWindow_ActivationStatusChanged(bool isActivated)
        {
            if (isActivated)
            {
                remainingTimeTimer.Stop();
                RemainingStatusBar.Visibility = Visibility.Hidden;
                ActivationLabel.Visibility = Visibility.Hidden;
                open.IsEnabled = true;
                btnOpen.IsEnabled = true;
                if (IsGameActivated && (GameStage == GameStages.GameStarted || GameStage == GameStages.OpenedGameStarted))
                    saveas.IsEnabled = true;
            }
            else
            {
                remainingTimeTimer.Start();
                RemainingStatusBar.Visibility = Visibility.Visible;
                if (GameStage == GameStages.CardsDealing)
                    ActivationLabel.Visibility = Visibility.Visible;
                open.IsEnabled = false;
                btnOpen.IsEnabled = false;
                saveas.IsEnabled = false;
            }
        }

        private void MainWindow_GameStageChanged(GameStages stage)
        {
            switch (stage)
            {
                case GameStages.CardsDealing:
                    MainInfoLabel.Content = "Hello";
                    break;
                case GameStages.OrderAces:
                    moveJournal = new Stack<Move>();
                    _moveCount = 0;
                    _gameTime = 0;
                    moveCountStatus.Text = "0";
                    undo.IsEnabled = false;
                    saveas.IsEnabled = false;
                    remainingShuffle.IsEnabled = false;
                    DialogBorder.Visibility = Visibility.Hidden;
                    MainInfoLabel.Content = "Please order aces";
                    if (salute1.Visibility == Visibility.Visible)
                    {
                        salute1.Visibility = Visibility.Hidden;
                        salute2.Visibility = Visibility.Hidden;
                        salute3.Visibility = Visibility.Hidden;
                        salute4.Visibility = Visibility.Hidden;
                    }
                    CongratsGrid.Visibility = Visibility.Hidden;
                    break;
                case GameStages.GameStarted:
                    DialogBorder.Visibility = Visibility.Hidden;
                    ActivationLabel.Visibility = Visibility.Hidden;
                    if (IsGameActivated)
                        saveas.IsEnabled = true;
                    remainingShuffle.IsEnabled = true;
                    RemainingCardsShuffle = 2;
                    foreach (Card card in cards)
                        card.Visibility = Visibility.Visible;
                    foreach (var blank in blankSpaces)
                        blank.IsEnabled = true;
                    foreach (Card card in cards.Where(w => w.Rank != Ranks.Ace))
                    {
                        card.IsEnabled = true;
                        card.Opacity = 1;
                        card.Effect = null;
                    }
                    foreach (Card card in cards.Where(w => w.Rank == Ranks.Ace))
                    {
                        AdjustOtherCards(new State() { Element = card, Position = new Position { ActualColumn = card.ActualColumn, ActualRow = card.ActualRow } });
                    }
                    MainInfoLabel.Content = "Please order all cards";
                    gameTimeTimer.Start();
                    break;
                case GameStages.OpenedGameStarted:
                    if (moveJournal.Count!=0)
                        undo.IsEnabled = true;
                    else
                        undo.IsEnabled = false;
                    saveas.IsEnabled = true;
                    foreach (Card card in cards)
                        card.Visibility = Visibility.Visible;
                    foreach (var blank in blankSpaces)
                        blank.IsEnabled = true;
                    MainInfoLabel.Content = "Please order all cards";
                    gameTimeTimer.Start();
                    ActivationLabel.Visibility = Visibility.Hidden;
                    DialogBorder.Visibility = Visibility.Hidden;
                    if (salute1.Visibility == Visibility.Visible)
                    {
                        salute1.Visibility = Visibility.Hidden;
                        salute2.Visibility = Visibility.Hidden;
                        salute3.Visibility = Visibility.Hidden;
                        salute4.Visibility = Visibility.Hidden;
                    }
                    CongratsGrid.Visibility = Visibility.Hidden;
                    break;
                case GameStages.GameWon:
                    salute1.Visibility = Visibility.Visible;
                    salute2.Visibility = Visibility.Visible;
                    salute3.Visibility = Visibility.Visible;
                    salute4.Visibility = Visibility.Visible;
                    CongratsGrid.Visibility = Visibility.Visible;
                    undo.IsEnabled = false;
                    saveas.IsEnabled = false;
                    MainInfoLabel.Content = "Congratulatuions, you won";
                    SetRecord();
                    break;
                case GameStages.GameBlocked:
                    cards.ForEach(c => c.Visibility = Visibility.Hidden);
                    MainMenu.IsEnabled = false;
                    salute1.Visibility = Visibility.Hidden;
                    salute2.Visibility = Visibility.Hidden;
                    salute3.Visibility = Visibility.Hidden;
                    salute4.Visibility = Visibility.Hidden;
                    CongratsGrid.Visibility = Visibility.Hidden;
                    DialogBorder.Visibility = Visibility.Hidden;
                    MainInfoLabel.Visibility = Visibility.Hidden;
                    UserNameLabel.Visibility = Visibility.Hidden;
                    remainingTimeTimer.Stop();
                    gameTimeTimer.Stop();
                    TimeIsUpGrid.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }
        }

        public MainWindow(string userName, bool isActivated) : this()
        {
            UserName = userName;
            UserNameLabel.Content = UserName;
            IsGameActivated = isActivated;
        }

        private void Current_Deactivated(object sender, EventArgs e)
        {
            AtOldPlace();
        }

        BlankSpace GetAvailableBlankSpace(Card card)
        {
            Rect selectedCardRect = new Rect(card.Margin.Left, card.Margin.Top, card.ActualWidth, card.ActualHeight);
            Dimension dimension = Dimension.GetRowColumnDimension(MainGrid);

            var blankSpace =
                blankSpaces.FirstOrDefault(f => new Rect(f.ActualColumn * dimension.Width, f.ActualRow * dimension.Height, f.ActualWidth, f.ActualHeight)
                                              .IntersectsWith(selectedCardRect));
            if (blankSpace == blankSpaces[0])
            {

            }
            return blankSpace;
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {
            if (SelectedCard != null)
            {
                double delX = Mouse.GetPosition(MainGrid).X - xD;
                double delY = Mouse.GetPosition(MainGrid).Y - yD;
                SelectedCard.Margin = new Thickness(SelectedCard.Margin.Left + delX,
                                                    SelectedCard.Margin.Top + delY,
                                                    0,
                                                    0);
                xD = Mouse.GetPosition(MainGrid).X;
                yD = Mouse.GetPosition(MainGrid).Y;

                var blankSpace = GetAvailableBlankSpace(SelectedCard);
                blankSpaces.ForEach(f => f.Background = Brushes.Transparent);
                if (blankSpace != null)
                {
                    blankSpace.Background = Brushes.AliceBlue;
                    BlurEffect blankEffect = new BlurEffect();
                    blankEffect.Radius = 30;
                    blankSpace.Effect = blankEffect;
                }
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (SelectedCard == null && e.LeftButton == MouseButtonState.Pressed && e.ChangedButton == MouseButton.Left)
            {
                SelectedCard = (Card)sender;
                SelectedCard.StartMove(MainGrid);
                SelectedCard.MouseUp += Image_MouseUp;
                xD = Mouse.GetPosition(MainGrid).X;
                yD = Mouse.GetPosition(MainGrid).Y;
                Mouse.Capture(SelectedCard);

                DropShadowEffect dse = new DropShadowEffect();
                dse.Direction = 300;
                dse.Color = Color.FromArgb(40, 40, 40, 40);
                dse.ShadowDepth = 30;
                dse.BlurRadius = 20;
                SelectedCard.Effect = dse;
            }
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released && e.ChangedButton == MouseButton.Left)
            {
                blankSpaces.ForEach(f => f.Background = Brushes.Transparent);
                Canvas.SetZIndex(SelectedCard, 0);
                var blankSpace = GetAvailableBlankSpace(SelectedCard);

                if (blankSpace != null)
                {

                    var previousCard = cards.FirstOrDefault(f => f.ActualColumn == blankSpace.ActualColumn - 1 && f.ActualRow == blankSpace.ActualRow);
                    var nextCard = cards.FirstOrDefault(f => f.ActualColumn == blankSpace.ActualColumn + 1 && f.ActualRow == blankSpace.ActualRow);

                    // place correct card
                    if (GameStage == GameStages.OrderAces
                        || (previousCard != null && previousCard == SelectedCard.PreviousCard)
                        || (nextCard != null && nextCard == SelectedCard.NextCard))
                    {
                        SelectedCard.MouseUp -= Image_MouseUp;
                        State blankSpaceState = new State()
                        {
                            Element = blankSpace,
                            Position = new Position() { ActualColumn = blankSpace.ActualColumn, ActualRow = blankSpace.ActualRow }
                        };
                        State selectedCardState = new State()
                        {
                            Element = SelectedCard,
                            Position = new Position() { ActualColumn = SelectedCard.ActualColumn, ActualRow = SelectedCard.ActualRow }
                        };
                        Move move = new Move() { blankSpaceState, selectedCardState };
                        SelectedCard.PlaceCard(blankSpace, blankSpace.ActualColumn, blankSpace.ActualRow);
                        correctSound?.Play();

                        //moves log
                        if (GameStage == GameStages.GameStarted || GameStage == GameStages.OpenedGameStarted)
                        {
                            moveJournal.Push(move);
                            _moveCount++;
                            moveCountStatus.Text = _moveCount.ToString();
                            if (undo.IsEnabled == false)
                                undo.IsEnabled = true;
                        }

                        //block correct cards 
                        AdjustOtherCards(selectedCardState);

                        if (SelectedCard != null)
                        {
                            // TODO bag because of ReleaseMouseCapture
                            SelectedCard.ReleaseMouseCapture();
                            SelectedCard.Effect = null;
                        }
                        SelectedCard = null;

                        //game won
                        if ((GameStage == GameStages.GameStarted || GameStage == GameStages.OpenedGameStarted)
                            && cards.Where(c => c.Rank == Ranks.King).All(p => p.IsEnabled == false))
                        {
                            cards.ForEach(c => c.Visibility = Visibility.Hidden);
                            GameStage = GameStages.GameWon;
                        }
                    }
                    else
                    {
                        AtOldPlace();
                    }

                    if (GameStage == GameStages.OrderAces)
                    {
                        bool changeStage = cards.Where(w => w.Rank == Ranks.Ace).All(a => a.ActualColumn == 0);
                        if (changeStage)
                        {
                            GameStage = GameStages.GameStarted;
                        }
                    }
                }
                else
                {
                    AtOldPlace();
                }
            }
        }

        public void AtOldPlace()
        {
            if (SelectedCard != null)
            {
                SelectedCard.ReleaseMouseCapture();
                blankSpaces.ForEach(f => f.Background = Brushes.Transparent);
                Canvas.SetZIndex(SelectedCard, 0);
                SelectedCard.ToOldPlace();
                errorSound?.Play();
                SelectedCard.MouseUp -= Image_MouseUp;
                SelectedCard.Effect = null;
                SelectedCard = null;
            }
        }

        public void ShuffleRemainingCards()
        {
            if (SelectedCard == null && (GameStage == GameStages.GameStarted || GameStage == GameStages.OpenedGameStarted) && RemainingCardsShuffle > 0)
            {
                List<Card> lastDisCards = cards.Where(c => c.IsEnabled == false && c.NextCard != null && c.NextCard.IsEnabled == true).ToList();
                foreach (Card lastDisCard in lastDisCards)
                {
                    var firstCard = cards.FirstOrDefault(c => c.ActualRow == lastDisCard.ActualRow
                                                         && c.ActualColumn - 1 == lastDisCard.ActualColumn);
                    if (firstCard != null)
                    {
                        BlankSpace freeBlankSpace = null;
                        foreach (BlankSpace blankSpace in blankSpaces)
                        {
                            if (!lastDisCards.Any(c => c.ActualColumn + 1 == blankSpace.ActualColumn &&
                                                            c.ActualRow == blankSpace.ActualRow))
                            {
                                freeBlankSpace = blankSpace;
                            }
                        }
                        int tempCoulumn = firstCard.ActualColumn;
                        int tempRow = firstCard.ActualRow;
                        firstCard.ActualColumn = freeBlankSpace.ActualColumn;
                        firstCard.ActualRow = freeBlankSpace.ActualRow;
                        freeBlankSpace.ActualColumn = tempCoulumn;
                        freeBlankSpace.ActualRow = tempRow;
                    }
                }
                List<Card> remainingCards = cards.Where(c => c.IsEnabled == true).ToList();
                MixImages(remainingCards);
                moveJournal = new Stack<Move>();
                undo.IsEnabled = false;
                RemainingCardsShuffle--;
            }
        }

        public void MixImages(List<Card> source)
        {
            rnd = new Random();
            for (int i = 0; i < source.Count; i++)
            {
                int _card1 = rnd.Next(0, source.Count);
                int _card2 = rnd.Next(0, source.Count);
                int _card1column = source[_card1].ActualColumn;
                int _card1row = source[_card1].ActualRow;
                source[_card1].ActualColumn = source[_card2].ActualColumn;
                source[_card1].ActualRow = source[_card2].ActualRow;
                source[_card2].ActualColumn = _card1column;
                source[_card2].ActualRow = _card1row;
            }
        }

        public void NewGame()
        {
            if (SelectedCard == null && GameStage != GameStages.GameBlocked)
            {
                GameStage = GameStages.OrderAces;
                for (int j = 0; j < 4; j++)
                {
                    blankSpaces[j].ActualColumn = 0;
                    blankSpaces[j].ActualRow = j;
                }
                foreach (Card card in cards)
                {
                    card.IsEnabled = false;
                    card.Opacity = 0.8;
                    card.Effect = newGameBLur;
                    card.ActualColumn = 1 + (int)card.Rank;
                    card.ActualRow = (int)card.Suit;
                    card.Visibility = Visibility.Visible;
                    if (card.Rank == Ranks.Ace)
                    {
                        card.IsEnabled = true;
                        card.Opacity = 1;
                        card.Effect = null;
                    }
                }
                MixImages(cards);
                ActivationLabel.Visibility = Visibility.Hidden;
                _gameTime = 0;
                gameTimeStatus.Text = _gameTime.ToString();
                gameTimeTimer.Stop();
            }
        }

        private void SettingsClick(object sender, RoutedEventArgs e)
        {
            Settings settings = new Settings(this);
            settings.ShowDialog();
        }

        private void MenuActivationClick(object sender, RoutedEventArgs e)
        {
            Activation activation = new Activation(this);
            activation.ShowDialog();
        }

        private void MenuAboutClick(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }

        private void MenuShuffleRemainingCards(object sender, RoutedEventArgs e)
        {
            ShuffleRemainingCards();
        }

        private void NewGameClick(object sender, RoutedEventArgs e)
        {
            NewGame();
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void GameSaveClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "pgnn file (*.pgnn)|*.pgnn";

            if (saveFileDialog.ShowDialog() == true)
            {
                List<object> blankSpaceSerialize = new List<object>();
                foreach (var blank in blankSpaces)
                {
                    blankSpaceSerialize.Add(blank.ActualColumn);
                    blankSpaceSerialize.Add(blank.ActualRow);
                }

                List<object> cardsSerialize = new List<object>();
                foreach (var card in cards)
                {
                    cardsSerialize.Add(card.ActualColumn);
                    cardsSerialize.Add(card.ActualRow);
                    cardsSerialize.Add(card.IsEnabled);
                    cardsSerialize.Add(card.Opacity);
                }

                List<object> journalSerialize = new List<object>();
                foreach (Move move in moveJournal)
                {
                    journalSerialize.Add(blankSpaces.IndexOf((BlankSpace)move[0].Element));
                    journalSerialize.Add(move[0].Position.ActualColumn);
                    journalSerialize.Add(move[0].Position.ActualRow);

                    journalSerialize.Add(move[1].Position.ActualColumn);
                    journalSerialize.Add(move[1].Position.ActualRow);
                    journalSerialize.Add(((Card)move[1].Element).Suit);
                    journalSerialize.Add(((Card)move[1].Element).Rank);
                }

                List<object> gameOtherInfo = new List<object>();
                gameOtherInfo.Add(_moveCount);
                gameOtherInfo.Add(_gameTime);
                gameOtherInfo.Add(_remainingTime);
                gameOtherInfo.Add(_remainingCardsShuffle);

                List<List<object>> saveFile = new List<List<object>>();
                saveFile.Add(blankSpaceSerialize);
                saveFile.Add(cardsSerialize);
                saveFile.Add(journalSerialize);
                saveFile.Add(gameOtherInfo);

                BinaryFormatter formatter = new BinaryFormatter();
                using (FileStream fs = new FileStream(saveFileDialog.FileName, FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, saveFile);
                }
            }
        }

        private void GameOpenClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "pgnn file (*.pgnn)|*.pgnn";
                if (openFileDialog.ShowDialog() == true)
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    using (FileStream fs = new FileStream(openFileDialog.FileName, FileMode.OpenOrCreate))
                    {
                        List<List<object>> openFile = (List<List<object>>)formatter.Deserialize(fs);

                        for (int i = 0; i < blankSpaces.Count; i++)
                        {
                            blankSpaces[i].ActualColumn = (int)openFile[0][i * 2];
                            blankSpaces[i].ActualRow = (int)openFile[0][i * 2 + 1];
                        }
                        for (int i = 0; i < cards.Count; i++)
                        {
                            cards[i].ActualColumn = (int)openFile[1][i * 4];
                            cards[i].ActualRow = (int)openFile[1][i * 4 + 1];
                            cards[i].IsEnabled = (bool)openFile[1][i * 4 + 2];
                            cards[i].Opacity = (double)openFile[1][i * 4 + 3];
                        }

                        moveJournal = new Stack<Move>();
                        for (int i = openFile[2].Count - 1; i >= 0; i--)
                        {
                            var cardRank = (Ranks)openFile[2][i--];
                            var cardSuit = (Suits)openFile[2][i--];
                            var cardRow = (int)openFile[2][i--];
                            var cardColumn = (int)openFile[2][i--];

                            var blankSpaceRow = (int)openFile[2][i--];
                            var blankSpaceColumn = (int)openFile[2][i--];
                            var blankSpaceIndex = (int)openFile[2][i];

                            State blankSpaceState = new State()
                            {
                                Element = blankSpaces[blankSpaceIndex],
                                Position = new Position() { ActualColumn = blankSpaceColumn, ActualRow = blankSpaceRow }
                            };
                            State selectedCardState = new State()
                            {
                                Element = cards.FirstOrDefault(c => c.Rank == cardRank && c.Suit == cardSuit),
                                Position = new Position() { ActualColumn = cardColumn, ActualRow = cardRow }
                            };
                            Move move = new Move() { blankSpaceState, selectedCardState };
                            moveJournal.Push(move);
                        }

                        _moveCount = (int)openFile[3][0];
                        moveCountStatus.Text = _moveCount.ToString();
                        _gameTime = (int)openFile[3][1];
                        gameTimeStatus.Text = _gameTime.ToString();
                        _remainingTime = (int)openFile[3][2];
                        remainingTimeStatus.Text = _remainingTime.ToString();
                        _remainingCardsShuffle = (int)openFile[3][3];
                        RemainingCardsShuffle = _remainingCardsShuffle;
                    }
                    GameStage = GameStages.OpenedGameStarted;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Can't open the file.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Undo()
        {
            if (moveJournal.Count != 0 && (GameStage == GameStages.GameStarted || GameStage == GameStages.OpenedGameStarted))
            {
                var move = moveJournal.Pop();

                _moveCount++;
                moveCountStatus.Text = _moveCount.ToString();
                if (moveJournal.Count != 0)
                    undo.IsEnabled = false;

                foreach (State state in move)
                {
                    state.Element.ActualColumn = state.Position.ActualColumn;
                    state.Element.ActualRow = state.Position.ActualRow;
                    AdjustOtherCards(state);
                }

            }
            else
            {
                undo.IsEnabled = false;
            }
        }

        private void MenuUndoClick(object sender, RoutedEventArgs e)
        {
            Undo();
        }

        public void AdjustOtherCards(State state)
        {
            IPosition element = state.Element;

            if (element is Card)
            {
                Card currentCard = element as Card;

                if (currentCard.PreviousCard != null && currentCard.ActualColumn - 1 == currentCard.PreviousCard.ActualColumn
                 && currentCard.ActualRow == currentCard.PreviousCard.ActualRow)
                {
                    currentCard.IsEnabled = currentCard.PreviousCard.IsEnabled;
                    currentCard.Opacity = currentCard.PreviousCard.Opacity;
                }
                else if (currentCard.PreviousCard == null && currentCard.ActualColumn == 0)
                {
                    currentCard.IsEnabled = false;
                    currentCard.Opacity = 0.8;
                }
                else
                {
                    currentCard.IsEnabled = true;
                    currentCard.Opacity = 1;
                }

                currentCard = currentCard.NextCard;
                while (currentCard != null)
                {
                    if (currentCard.PreviousCard != null && currentCard.ActualColumn - 1 == currentCard.PreviousCard.ActualColumn
                                                         && currentCard.ActualRow == currentCard.PreviousCard.ActualRow)
                    {
                        currentCard.IsEnabled = currentCard.PreviousCard.IsEnabled;
                        currentCard.Opacity = currentCard.PreviousCard.Opacity;
                    }
                    else if (GameStage == GameStages.GameStarted || GameStage == GameStages.OpenedGameStarted)
                    {
                        currentCard.IsEnabled = true;
                        currentCard.Opacity = 1;
                    }
                    if (currentCard.NextCard == null || currentCard.NextCard.ActualColumn - 1 != currentCard.ActualColumn
                                                     || currentCard.NextCard.ActualRow != currentCard.ActualRow)
                    {
                        break;
                    }
                    currentCard = currentCard.NextCard;
                }
            }
        }

        public void MenuTest(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < cards.Count - 4; i += 4)
            {
                cards[i].ActualColumn = i / 4;
                cards[i].ActualRow = 0;
                cards[i].IsEnabled = false;
                cards[i].Opacity = 0.8;
                cards[i + 1].ActualColumn = i / 4;
                cards[i + 1].ActualRow = 1;
                cards[i + 1].IsEnabled = false;
                cards[i + 1].Opacity = 0.8;
                cards[i + 2].ActualColumn = i / 4;
                cards[i + 2].ActualRow = 2;
                cards[i + 2].IsEnabled = false;
                cards[i + 2].Opacity = 0.8;
                cards[i + 3].ActualColumn = i / 4;
                cards[i + 3].ActualRow = 3;
                cards[i + 3].IsEnabled = false;
                cards[i + 3].Opacity = 0.8;
            }
            for (int i = 0; i < 4; i++)
            {
                cards[48 + i].ActualColumn = 13;
                cards[48 + i].ActualRow = i;
                blankSpaces[i].ActualColumn = 12;
                blankSpaces[i].ActualRow = i;
            }
        }

        private void realTimeTick(object sender, EventArgs e)
        {
            TimeLabel.Content = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
        }

        private void gameTimeTick(object sender, EventArgs e)
        {
            _gameTime++;
            gameTimeStatus.Text = _gameTime.ToString();
        }
        private void remainingTimeTick(object sender, EventArgs e)
        {
            _remainingTime--;
            remainingTimeStatus.Text = _remainingTime.ToString();
            if (_remainingTime <= 0)
            {
                GameStage = GameStages.GameBlocked;
            }
        }

        private byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public string GetString(byte[] bytes)
        {
            if (bytes != null)
            {
                char[] chars = new char[bytes.Length / sizeof(char)];
                System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
                return new string(chars);
            }
            return null;
        }

        private void Record_Click(object sender, RoutedEventArgs e)
        {
            PaganiniRegInfo regInfo = GetRegistryInfo();
            MessageBox.Show($"Timer record is {regInfo.TimeRecord} sec. by {regInfo.TimeRecordsPerson}." +
                $"\nMoves record is {regInfo.MovesRecord} moves by {regInfo.MovesRecordsPerson}.", "Records", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        private PaganiniRegInfo GetRegistryInfo()
        {
            PaganiniRegInfo regInfo = new PaganiniRegInfo();
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey PaganiniRecordInfo = currentUserKey.OpenSubKey("PaganiniRecordInfo");
            RegistryKey TimeRecordInfo = PaganiniRecordInfo?.OpenSubKey("TimeRecordInfo");
            RegistryKey MovesRecordInfo = PaganiniRecordInfo?.OpenSubKey("MovesRecordInfo");

            var _timeRecord = TimeRecordInfo?.GetValue("TimeRecord") as byte[];
            var _timeRecordsPerson = TimeRecordInfo?.GetValue("TimeRecordsPerson") as byte[];
            var _movesRecord = MovesRecordInfo?.GetValue("MovesRecord") as byte[];
            var _movesRecordsPerson = MovesRecordInfo?.GetValue("MovesRecordsPerson") as byte[];

            regInfo.TimeRecord = GetString(_timeRecord);
            regInfo.TimeRecordsPerson = GetString(_timeRecordsPerson);
            regInfo.MovesRecord = GetString(_movesRecord);
            regInfo.MovesRecordsPerson = GetString(_movesRecordsPerson);
            return regInfo;
        }

        private void SetRecord()
        {
            switch (RecordMode)
            {
                case GameRecordMode.Move:
                    int.TryParse(GetRegistryInfo().MovesRecord, out int recordMove);
                    if (_moveCount < recordMove)
                    {
                        var msgBoxResult = MessageBox.Show("Do you want to save your move record?", "Record save", MessageBoxButton.YesNo);
                        if (msgBoxResult == MessageBoxResult.Yes)
                        {
                            RegistryKey currentUserKey = Registry.CurrentUser;
                            RegistryKey PaganiniRecordInfo = currentUserKey.CreateSubKey("PaganiniRecordInfo");
                            RegistryKey MovesRecordInfo = PaganiniRecordInfo.CreateSubKey("MovesRecordInfo");
                            MovesRecordInfo.SetValue("MovesRecord", GetBytes(_moveCount.ToString()));
                            MovesRecordInfo.SetValue("MovesRecordsPerson", GetBytes(UserName));
                            PaganiniRecordInfo.Close();
                        }
                    }
                    break;
                case GameRecordMode.Time:
                    int.TryParse(GetRegistryInfo().TimeRecord, out int recordTime);
                    if (_gameTime < recordTime)
                    {
                        var msgBoxResult = MessageBox.Show("Do you want to save your time record?", "Record save", MessageBoxButton.YesNo);
                        if (msgBoxResult == MessageBoxResult.Yes)
                        {
                            RegistryKey currentUserKey = Registry.CurrentUser;
                            RegistryKey PaganiniRecordInfo = currentUserKey.CreateSubKey("PaganiniRecordInfo");
                            RegistryKey TimeRecordInfo = PaganiniRecordInfo.CreateSubKey("TimeRecordInfo");
                            TimeRecordInfo.SetValue("TimeRecord", GetBytes(_gameTime.ToString()));
                            TimeRecordInfo.SetValue("TimeRecordsPerson", GetBytes(UserName));
                            PaganiniRecordInfo.Close();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
