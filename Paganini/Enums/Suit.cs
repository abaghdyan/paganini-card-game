﻿namespace Paganini.Enums
{
    public enum Suits
    {
        Hearts,
        Clubs,
        Diamonds,
        Spades
    }
}
