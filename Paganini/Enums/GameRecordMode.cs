﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paganini.Enums
{
    public enum GameRecordMode
    {
        None,
        Move,
        Time
    }
}
