﻿namespace Paganini
{
    public enum GameStages
    {
        CardsDealing,
        OrderAces,
        GameStarted,
        OpenedGameStarted,
        GameWon,
        GameBlocked
    }
}