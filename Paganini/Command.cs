﻿using System;
using System.Windows.Input;

namespace Paganini
{
    public class Command : ICommand
    {
        private readonly Action _action;
        public           bool   CanExecuteCommand { get; set; }
        public Command(Action action)
        {
            CanExecuteCommand = true;
            _action           = action;
        }
        public bool CanExecute(object parameter)
        {
            return CanExecuteCommand;
        }

        public void Execute(object parameter)
        {
            _action();
        }

        public event EventHandler CanExecuteChanged;
    }
}