﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Paganini
{
    /// <summary>
    /// Логика взаимодействия для EnterUserName.xaml
    /// </summary>
    public partial class EnterUserName : Window
    {
        public EnterUserName()
        {
            InitializeComponent();

            Opacity = 0;
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey PaganiniInfo = currentUserKey.OpenSubKey("PaganiniInfo");
            var sKey = PaganiniInfo?.GetValue("SerialKey") as byte[];
            if (sKey!=null && Activation.IsValid(GetString(sKey)))
            {
                var userName = PaganiniInfo.GetValue("UserName") as byte[];
                OpenMainWindow(GetString(userName), true);
                PaganiniInfo.Close();
            }
        }

        public string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (UserNameText.Text.All(c=> (c >= 65 && c <= 90) || (c>=97 && c <= 122)))
            {
                OpenMainWindow(UserNameText.Text, false);
            }
            else
            {
                InfoLabel.Visibility = Visibility.Visible;
                UserNameText.Background = Brushes.Red;
            }
        }

        private void OpenMainWindow(string userName, bool isActivated)
        {
            MainWindow main = new MainWindow(userName, isActivated);
            main.Show();
            this.Close();
        }

        private void UserNameText_GotFocus(object sender, RoutedEventArgs e)
        {
            DropShadowEffect dse = new DropShadowEffect();
            dse.Color = Color.FromRgb(255, 255, 255);
            dse.ShadowDepth = 0;
            dse.BlurRadius = 20;
            UserNameText.Effect = dse;
        }

        private void UserNameText_LostFocus(object sender, RoutedEventArgs e)
        {
            UserNameText.Effect = null;
        }

        private void UserNameText_TextChanged(object sender, TextChangedEventArgs e)
        {
            InfoLabel.Visibility = Visibility.Hidden;
            UserNameText.Background = Brushes.White;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }
    }
}
