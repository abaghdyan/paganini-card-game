﻿using Paganini.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Paganini
{
    /// <summary>
    /// Логика взаимодействия для Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        BlurEffect imageBlur = new BlurEffect();
        DropShadowEffect imageShadow = new DropShadowEffect();
        Image settingsMainImage = new Image();
        GameRecordMode settingsMainRecordMode;
        bool _isMainSoundOn;

        List<Image> images = new List<Image>();
        private MainWindow parental;
        public Settings(MainWindow parental)
        {
            this.parental = parental;
            InitializeComponent();

            imageBlur.Radius = 5;

            imageShadow.Color = Color.FromRgb(255, 0, 0);
            imageShadow.ShadowDepth = 0;
            imageShadow.Direction = 0;
            imageShadow.BlurRadius = 30;

            images.Add(Image1);
            images.Add(Image2);
            images.Add(Image3);
            images.Add(Image4);
            images.Add(Image5);
            images.Add(Image6);
            images.ForEach(i => i.Effect = imageBlur);

            userName.Text = parental.UserName;
            settingsMainRecordMode = parental.RecordMode;
            switch (parental.RecordMode)
            {
                case GameRecordMode.Move:
                    Move.IsChecked = true;
                    break;
                case GameRecordMode.Time:
                    Time.IsChecked = true;
                    break;
                default:
                    break;
            }
            _isMainSoundOn = parental.IsSoundOn;
            currentTime.IsChecked = parental.ShowCurrentTime;
            if (parental.IsSoundOn)
            {
                SoundToggle.DoOn();
            }
            if (MainWindow.IsGameActivated)
            {
                userName.IsEnabled = false;
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var selectedImage = sender as Image;
            if (selectedImage != null)
            {
                if (selectedImage.Effect != imageShadow)
                {
                    images.ForEach(i => i.Effect = imageBlur);
                    selectedImage.Effect = imageShadow;
                    settingsMainImage.Source = selectedImage.Source;
                }
                else
                {
                    images.ForEach(i => i.Effect = imageBlur);
                    settingsMainImage.Source = null;
                }
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (userName.Text.All(c => (c >= 65 && c <= 90) || (c >= 97 && c <= 122)) || userName.Text=="")
            {
                if (settingsMainImage.Source != null)
                {
                    parental.mainBackground.ImageSource = settingsMainImage.Source;
                }
                parental.UserName = userName.Text;
                parental.UserNameLabel.Content = userName.Text;
                parental.RecordMode = settingsMainRecordMode;
                parental.IsSoundOn = _isMainSoundOn;
                parental.ShowCurrentTime = (bool)currentTime.IsChecked;
                this.Close();
            }
            else
            {
                InfoLabel.Visibility = Visibility.Visible;
                userName.Background = Brushes.Red;
            }
        }

        private void None_Checked(object sender, RoutedEventArgs e)
        {
            settingsMainRecordMode = GameRecordMode.None;
        }

        private void Move_Checked(object sender, RoutedEventArgs e)
        {
            settingsMainRecordMode = GameRecordMode.Move;
        }

        private void Time_Checked(object sender, RoutedEventArgs e)
        {
            settingsMainRecordMode = GameRecordMode.Time;
        }

        private void SoundToggle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _isMainSoundOn = SoundToggle.Toggled1;
        }

        private void UserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (InfoLabel!=null && InfoLabel.Visibility == Visibility.Visible)
            {
                InfoLabel.Visibility = Visibility.Hidden;
                userName.Background = Brushes.White;
            }
        }
    }
}
