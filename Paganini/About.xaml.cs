﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Paganini
{
    /// <summary>
    /// Логика взаимодействия для About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();
            InfoText.Content = "\nIDE:\tWPF C#\n\nVersion:\t1.0.0.0\n\nCreator:\tabaghdyan\n\nEmail:\tabaghdyan@gmail.com";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Image_MouseEnter(object sender, MouseEventArgs e)
        {
            aboutImg.Margin = new Thickness(10, 30, 10, 30);
            aboutImg.Opacity = 1;
        }

        private void AboutImg_MouseLeave(object sender, MouseEventArgs e)
        {
            aboutImg.Margin = new Thickness(15, 40, 15, 40);
            aboutImg.Opacity = 0.6;
        }
    }
}
