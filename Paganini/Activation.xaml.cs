﻿using Microsoft.Win32;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;

namespace Paganini
{
    public partial class Activation : Window
    {
        DropShadowEffect dse = new DropShadowEffect();
        private MainWindow parental;

        public Activation(MainWindow parental)
        {
            this.parental = parental;
            InitializeComponent();

            UName.Text = parental.UserName;
            if (MainWindow.IsGameActivated==true)
            {
                UName.IsEnabled = false;
                PKey.IsEnabled = false;
                Activate.IsEnabled = false;
                Deactivate.Visibility = Visibility.Visible;
                PKey.Text = "XXXX-XXXX-XXXX-XXXX";
                InfoLabel.Foreground = Brushes.Green;
                InfoLabel.Content = "Paganini is activated.";
            }
            pbStatus.ValueChanged += PbStatus_ValueChanged;
            pbStatus1.ValueChanged += PbStatus1_ValueChanged;
            pbStatus2.ValueChanged += PbStatus2_ValueChanged;

            dse.Direction = 0;
            dse.ShadowDepth = 10;
            dse.Color = Color.FromRgb(10, 187, 139);
            dse.ShadowDepth = 0;
            dse.BlurRadius = 40;
        }
        private byte[] GetBytes(string str)
        {
            if (str!= null)
            {
                byte[] bytes = new byte[str.Length * sizeof(char)];
                System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
                return bytes;
            }
            return null;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (UName.Text.Length == 0)
            {
                UName.Background = Brushes.Red;
                InfoLabel.Foreground = Brushes.Red;
                InfoLabel.Content = "Please enter your name!";
            }
            else if (!UName.Text.All(c => (c >= 65 && c <= 90) || (c >= 97 && c <= 122)))
            {
                UName.Background = Brushes.Red;
                InfoLabel.Foreground = Brushes.Red;
                InfoLabel.Content = "Please enter characters A-Z.";
            }
            else if (IsValid(PKey.Text))
            {
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey PaganiniInfo = currentUserKey.CreateSubKey("PaganiniInfo");
                PaganiniInfo.SetValue("UserName", GetBytes(UName.Text));
                PaganiniInfo.SetValue("SerialKey", GetBytes(PKey.Text));
                PaganiniInfo.SetValue("RegisteredAt", GetBytes(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")));
                PaganiniInfo.Close();

                LoadProgressBar(pbStatus);
                InfoLabel.Foreground = Brushes.Green;
                InfoLabel.Content = "Paganini was activated!";
                parental.UserName = UName.Text;
                parental.UserNameLabel.Content = UName.Text;
                UName.IsEnabled = false;
                PKey.IsEnabled = false;
                Activate.IsEnabled = false;
                MainWindow.IsGameActivated = true;
            }
            else
            {
                PKey.Background = Brushes.Red;
                InfoLabel.Foreground = Brushes.Red;
                InfoLabel.Content = "Wrong produkt key!";
            }

        }
        private void LoadProgressBar(ProgressBar progBar)
        {
            Duration dur = new Duration(TimeSpan.FromMilliseconds(800));
            DoubleAnimation dbani = new DoubleAnimation(300.0, dur);
            progBar.BeginAnimation(ProgressBar.ValueProperty, dbani);
        }

        public static bool IsValid(string key)
        {
            try
            {
                string sKey = key.Replace("-", "");
                char zero = sKey[0];
                char four = sKey[4];
                char three = sKey[3];

                char eleven = sKey[11];
                char eight = sKey[8];
                char seven = sKey[7];

                int one = (int)char.GetNumericValue(sKey[1]);
                int two = (int)char.GetNumericValue(sKey[2]);
                int five = (int)char.GetNumericValue(sKey[5]);
                int six = (int)char.GetNumericValue(sKey[6]);
                int nine = (int)char.GetNumericValue(sKey[9]);
                int ten = (int)char.GetNumericValue(sKey[10]);

                char twelve = sKey[12];
                char fifteen = sKey[15];
                int thirteen = (int)char.GetNumericValue(sKey[13]);
                int fourteen = (int)char.GetNumericValue(sKey[14]);

                return sKey.Length == 16
                           && three == (char)((zero + four) / 2)
                           && seven == (char)((eleven + eight) / 2)
                           && one == (seven * 3) % 10
                           && two == (eight * 7) % 10
                           && five == (zero * 3) % 10
                           && six == (eleven * 7) % 10
                           && nine == (three * 3) % 10
                           && ten == (four * 7) % 10
                           && twelve == (char)((zero + three + four + seven + eight + eleven) / 6)
                           && fifteen == (char)((four + eight) / 2)
                           && thirteen == (one + two + five) / 3
                           && fourteen == (six + nine + ten) / 3;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void UNamePKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            PKey.Background = null;
            UName.Background = null;
            InfoLabel.Content = "";
        }

        private void UName_LostFocus(object sender, RoutedEventArgs e)
        {
            UName.Effect = null;
        }

        private void UName_GotFocus(object sender, RoutedEventArgs e)
        {
            UName.Effect = dse;
        }

        private void PKey_GotFocus(object sender, RoutedEventArgs e)
        {
            PKey.Effect = dse;
        }

        private void PKey_LostFocus(object sender, RoutedEventArgs e)
        {
            PKey.Effect = null;
        }

        private void PbStatus_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (pbStatus.Value == 100)
            {
                LoadProgressBar(pbStatus1);
            }
        }

        private void PbStatus1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (pbStatus1.Value == 100)
            {
                LoadProgressBar(pbStatus2);
            }
        }

        private void PbStatus2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (pbStatus2.Value == 100)
            {
                LoadProgressBar(pbStatus3);
            }
        }

        private void Deactivate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RegistryKey currentUserKey = Registry.CurrentUser;
                currentUserKey.DeleteSubKey("PaganiniInfo");
                LoadProgressBar(pbStatus);
                Deactivate.IsEnabled = false;
                InfoLabel.Foreground = Brushes.Green;
                InfoLabel.Content = "Paganini was deactivated.";
                MainWindow.IsGameActivated = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Something gone bad :(", "Error!!!");
            }
        }
    }
}
