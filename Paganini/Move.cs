﻿using System;
using System.Collections.Generic;

namespace Paganini
{
    [Serializable]
    public struct Position : IPosition
    {
        public int ActualColumn { get; set; }
        public int ActualRow { get; set; }
    }

    [Serializable]
    public class State
    {
        public IPosition Element { get; set; }
        public Position Position { get; set; }
    }

    [Serializable]
    public class Move : List<State>
    {

    }
}
