﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Paganini
{
    public class BlankSpace : Border, IPosition
    {
        private int _actualColumn;
        private int _actualRow;

        public BlankSpace()
        {
            BorderThickness = new Thickness();
            Margin          = new Thickness(10);
            BorderBrush     = Brushes.Black;
            Canvas.SetZIndex(this, 100);
        }

        public int ActualColumn
        {
            get { return _actualColumn; }
            set
            {
                Grid.SetColumn(this, value);
                _actualColumn = value;
            }
        }

        public int ActualRow
        {
            get { return _actualRow; }
            set
            {
                Grid.SetRow(this, value);
                _actualRow = value;
            }
        }
    }
}