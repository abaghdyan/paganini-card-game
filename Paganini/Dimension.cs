﻿using System.Windows.Controls;

namespace Paganini
{
    public class Dimension
    {
        public double Width  { get; set; }
        public double Height { get; set; }
        public static Dimension GetRowColumnDimension(Grid element)
        {
            Dimension dimension = new Dimension {Height = element.ActualHeight / 4, Width = element.ActualWidth / 14};
            return dimension;
        }
    }
}