﻿namespace Paganini
{
    public interface IPosition
    {
        int ActualColumn { get; set; }
        int ActualRow    { get; set; }
    }
}