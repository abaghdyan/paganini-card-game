﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paganini
{
    public class PaganiniRegInfo
    {
        public string TimeRecord;
        public string TimeRecordsPerson;
        public string MovesRecord;
        public string MovesRecordsPerson;
    }
}
