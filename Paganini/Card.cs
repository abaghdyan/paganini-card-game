﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Paganini.Enums;

namespace Paganini
{
    class CardInfo
    {

    }
    [Serializable]
    public class Card : Image, IPosition
    {
        private int _actualColumn;
        private int _actualRow;
        public Card PreviousCard { get; set; }
        public Card NextCard { get; set; }
        public Card()
        {
        }
        public int ActualColumn
        {
            get { return _actualColumn; }
            set
            {
                Grid.SetColumn(this, value);
                _actualColumn = value;
            }
        }

        public int ActualRow
        {
            get { return _actualRow; }
            set
            {
                Grid.SetRow(this, value);
                _actualRow = value;
            }
        }
        public Ranks Rank { get; set; }
        public Suits Suit { get; set; }

        public Card(Ranks rank, Suits suit)
        {
            Rank = rank;
            Suit = suit;
        }

        public Card(Ranks rank) : this(rank, Suits.Spades)
        {
        }

        public override string ToString()
        {
            return (Rank + " " + Suit);
        }

        public BitmapImage GetSource()
        {
            return new BitmapImage(new Uri("Content/Cards/" + Suit + " " + Rank + ".png", UriKind.Relative));
        }

        public void StartMove(Grid MainGrid)
        {
            Canvas.SetZIndex(this, int.MaxValue);
            double accHeight = ActualHeight;
            double accWidth  = ActualWidth;
            Width  = accWidth;
            Height = accHeight;
            Dimension dimention = Dimension.GetRowColumnDimension(MainGrid);
            HorizontalAlignment = HorizontalAlignment.Left;
            VerticalAlignment   = VerticalAlignment.Top;
            Margin              = new Thickness(ActualColumn * dimention.Width + (dimention.Width - accWidth) / 2, ActualRow * dimention.Height + (dimention.Height - accHeight) / 2, 0, 0);
            Grid.SetRow(this, 0);
            Grid.SetColumn(this, 0);
            Grid.SetColumnSpan(this, 14);
            Grid.SetRowSpan(this, 4);
        }

        public void ToOldPlace()
        {
            HorizontalAlignment =  HorizontalAlignment.Stretch;
            VerticalAlignment   =  VerticalAlignment.Stretch;
            Margin              =  new Thickness(0);
            Grid.SetRow(this, ActualRow);
            Grid.SetColumn(this, ActualColumn);
            Grid.SetColumnSpan(this, 1);
            Grid.SetRowSpan(this, 1);
            Width  = double.NaN;
            Height = double.NaN;
        }

        public void PlaceCard(BlankSpace element, int mouseColumn, int mouseRow)
        {
            element.ActualColumn = ActualColumn;
            element.ActualRow    = ActualRow;
            ActualColumn         = mouseColumn;
            ActualRow            = mouseRow;
            HorizontalAlignment  = HorizontalAlignment.Stretch;
            VerticalAlignment    = VerticalAlignment.Stretch;
            Margin               = new Thickness(0);
            Grid.SetColumnSpan(this, 1);
            Grid.SetRowSpan(this, 1);
            Width  = double.NaN;
            Height = double.NaN;
        }
    }
}
