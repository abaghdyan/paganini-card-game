﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PaganiniKeyGenerator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly DoubleAnimation _oa;
        public MainWindow()
        {
            InitializeComponent();

            Opacity = 0;
            _oa = new DoubleAnimation();
            _oa.From = this.Opacity;
            _oa.To = 1;
            _oa.Duration = new Duration(TimeSpan.FromMilliseconds(400d));
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            this.Opacity = 0;
            BeginAnimation(OpacityProperty, _oa);
            Copy.IsEnabled = true;

            string key;
            Random rnd = new Random();

            char zero = (char)rnd.Next(65, 90);
            char four = (char)rnd.Next(65, 90);
            char three = (char)((zero + four) / 2);

            char eleven = (char)rnd.Next(65, 90);
            char eight = (char)rnd.Next(65, 90);
            char seven = (char)((eleven + eight) / 2);

            int one = (seven * 3) % 10;
            int two = (eight * 7) % 10;
            int five = (zero * 3) % 10;
            int six = (eleven * 7) % 10;
            int nine = (three * 3) % 10;
            int ten = (four * 7) % 10;

            char twelve = (char)((zero + three + four + seven + eight + eleven) / 6);
            char fifteen = (char)((four + eight) / 2);
            int thirteen = (one + two + five) / 3;
            int fourteen = (six + nine + ten) / 3;


            key = string.Format($"{zero}{one}{two}{three}-{four}{five}{six}{seven}-" +
                $"{eight}{nine}{ten}{eleven}-{twelve}{thirteen}{fourteen}{fifteen}");
            Key.Text = key;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BeginAnimation(OpacityProperty, _oa);
        }

        private void Copy_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(Key.Text);
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }
    }
}
